#include "config.h"
#include "MeshNet.h"
#include "Button.h"

MeshNet net({
    STATION_MODE, WIFI_CHANNEL,
    MESH_PORT, MESH_PREFIX, MESH_PASSWORD,
    STATION_SSID, STATION_PASSWORD, HOSTNAME,
    MESH_DEBUG_TYPES
});
Button sprocket({ STARTUP_DELAY, SERIAL_BAUD_RATE });

void setup() {
    sprocket.join(net);
}

void loop() {
    sprocket.loop();
    yield();
}