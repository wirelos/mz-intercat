    #ifndef __MESH_APP__
#define __MESH_APP__

#include <painlessMesh.h>
#include <Sprocket.h>
#include <MeshNet.h>

using namespace std;
using namespace std::placeholders;

class MeshApp : public Sprocket {
    public:
        Task someTask;
        MeshNet* net;
        MeshApp(SprocketConfig cfg) : Sprocket(cfg) {}
        Sprocket* activate(Scheduler* scheduler, Network* network) {
            net = static_cast<MeshNet*>(network);
            net->mesh.onReceive(bind(&MeshApp::receivedCallback,this, _1, _2));
            // add a task that sends stuff to the mesh
            someTask.set(TASK_SECOND * 5, TASK_FOREVER,
                bind(&MeshApp::heartbeat, this, net));
            scheduler->addTask(someTask);
            someTask.enable();
        } using Sprocket::activate;

        void heartbeat(MeshNet* network){
            String msg = "{ \"payload \": 1 }";
            network->broadcast(msg);
        }

        void receivedCallback( uint32_t from, String &msg ) {
            Serial.printf("startHere: Received from %u msg=%s\n", from, msg.c_str());
            // respond in receive callback can cause an endless loop when all nodes run the same firmware
            //String foo = String("cheerz back to ") + String(from);
            //net->broadcast(foo);
        }
        void loop() {
            net->update();
            scheduler.execute();
        }
};

#endif