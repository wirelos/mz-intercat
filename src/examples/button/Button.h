    #ifndef __MESH_APP__
#define __MESH_APP__

#include <painlessMesh.h>
#include <Sprocket.h>
#include <MeshNet.h>
#include <ArduinoJson.h>


using namespace std;
using namespace std::placeholders;

class Button : public Sprocket {
    public:
        Task btnTask;
        Task potTask;
        MeshNet* net;
        int pin;
        int pinA = 0;
        int potValuePrevious;
        int warnlevel = 0;
        int cycleEnd = 5;
        Button(SprocketConfig cfg) : Sprocket(cfg) {}
        Sprocket* activate(Scheduler* scheduler, Network* network) {
            pin = D2;
            pinA = A0;
            pinMode(pin, INPUT_PULLUP);
            net = static_cast<MeshNet*>(network);
            net->mesh.onReceive(bind(&Button::receivedCallback,this, _1, _2));
            btnTask.set(TASK_MILLISECOND * 250, TASK_FOREVER,
                bind(&Button::readPin, this, net));
            scheduler->addTask(btnTask);
            btnTask.enable();

            potTask.set(TASK_MILLISECOND * 100, TASK_FOREVER,
                bind(&Button::readPot, this, net));
            scheduler->addTask(potTask);
            potTask.enable();
        } using Sprocket::activate;

        void readPin(MeshNet* network){
            if(digitalRead(pin)){
                StaticJsonBuffer<200> jsonBuffer;
                JsonObject& root = jsonBuffer.createObject();
                String jsonString;

                root["action"] = "button";
                root["severity"] = potValuePrevious;
                root.printTo(jsonString);

                Serial.println(jsonString);
                network->broadcast(jsonString);
            }
        }

        void readPot(MeshNet* network){
            if(analogRead(pinA)){
                int potValue = map(analogRead(pinA) + 10, 1024 + 20, 0, 0, 6);
                if(potValue != potValuePrevious) {
                    StaticJsonBuffer<200> jsonBuffer;
                    JsonObject& root = jsonBuffer.createObject();
                    String jsonString;

                    root["action"] = "dial";
                    root["severity"] = potValue;
                    root.printTo(jsonString);

                    Serial.println(jsonString);
                    network->broadcast(jsonString);
                    potValuePrevious = potValue;
                }
                Serial.println("warnlevel: " + String(warnlevel));
                warnlevel++;
                if(warnlevel == cycleEnd) warnlevel = 0;
                network->broadcast(String(warnlevel));
            }
        }

        void receivedCallback( uint32_t from, String &msg ) {
            Serial.printf("button: Received from %u msg=%s\n", from, msg.c_str());
            // respond in receive callback can cause an endless loop when all nodes run the same firmware
            //String foo = String("cheerz back to ") + String(from);
            //net->broadcast(foo);
        }
        void loop() {
            net->update();
            scheduler.execute();
        }
};

#endif