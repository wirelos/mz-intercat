#ifndef __BRIDGE_CONFIG__
#define __BRIDGE_CONFIG__

// Scheduler config
#define _TASK_SLEEP_ON_IDLE_RUN
#define _TASK_STD_FUNCTION

// Chip config
#define SERIAL_BAUD_RATE    115200
#define STARTUP_DELAY       3000

// Mesh config
#define STATION_MODE        0 // 1 = connect to AP using STATION params
#define WIFI_CHANNEL        11
#define MESH_PORT           5555
#define MESH_PREFIX         "wirelos_contraption"
#define MESH_PASSWORD       "th3r31sn0sp00n"
#define STATION_SSID        "Th1ngs4P"
#define STATION_PASSWORD    "th3r31sn0sp00n"
#define HOSTNAME            "sprocket"
#define MESH_DEBUG_TYPES    ERROR | STARTUP | CONNECTION

#define MQTT_CLIENT_NAME    HOSTNAME
#define MQTT_BROKER         "citadel.lan"
#define MQTT_PORT           1883
#define MQTT_TOPIC_ROOT     "mesh"

#endif