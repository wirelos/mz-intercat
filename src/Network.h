#ifndef __NETWORK_H__
#define __NETWORK_H__

#include <Arduino.h>
#include <TaskSchedulerDeclarations.h>

class Network {
    public:
        uint32_t id = 0;
        Network(){}
        Scheduler* scheduler;
        virtual Network* init() { return this; };
        virtual Network* init(Scheduler* s) { scheduler = s; return init(); };
        virtual Network* connect() { return this; };
        virtual void update() {};
        virtual void broadcast(String msg){
            Serial.println("no-broadcast");
        };
        Network* setScheduler(Scheduler* s) {
            scheduler = s;
            return this;
        }
};

#endif