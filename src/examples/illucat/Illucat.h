#ifndef __MESH_APP__
#define __MESH_APP__

#include <painlessMesh.h>
#include <Sprocket.h>
#include <MeshNet.h>
#include "NeoPattern.h"

using namespace std;
using namespace std::placeholders;

#define ARRAY_LENGTH(array) sizeof(array)/sizeof(array[0])

enum PIXEL_MODES {BLACK = 0x000000, RED = 0xFF0000, GREEN = 0x00FF00, YELLOW = 0xffff00, BLUE = 0x0000FF, ORANGE = 0xffa500};
int CAT_MODES[] = {BLACK, RED, GREEN, YELLOW, BLUE, ORANGE};

struct NeoPixelConfig {
    int pin;
    int length;
    int mode;
};

class Illucat : public Sprocket {
    public:
        MeshNet* net;
        NeoPattern* pixels;
        int currentMode;
        
        Illucat(SprocketConfig cfg, NeoPixelConfig pixelCfg) : Sprocket(cfg) {
            pixels = new NeoPattern(pixelCfg.length, pixelCfg.pin, NEO_GRB + NEO_KHZ800, [](int pixels){});
            pixels->begin();
            pixels->setBrightness(64);
        }
        Sprocket* activate(Scheduler* scheduler, Network* network) {
            net = static_cast<MeshNet*>(network);
            net->mesh.onReceive(bind(&Illucat::messageReceived,this, _1, _2));
            // TODO default rainbow task
        } using Sprocket::activate;

        void messageReceived( uint32_t from, String &msg ) {
            Serial.printf("illucat: received from %u msg=%s\n", from, msg.c_str());
            StaticJsonBuffer<200> jsonBuffer;
            JsonObject& root = jsonBuffer.parseObject(msg);
            if (!root.success()) {
                Serial.println("parseObject() failed");
                return;
            }
            changeMode(root["severity"]);
            /* if(root["action"] == "pressed") {
            }
            if(root["action"] == "dial") {
                changeMode("0");
            } */
        }
        void changeMode(const char *mode){
            currentMode = atoi(mode);
            Serial.println(currentMode);
            pixels->ColorSet(CAT_MODES[currentMode]);
            pixels->show();
        }
        void setHexColor(const char *hex){
            int r, g, b;
            sscanf(hex, "%02x%02x%02x", &r, &g, &b);
            pixels->ColorSet(pixels->Color(r,g,b));
        }
        void loop() {
            net->update();
            scheduler.execute();
        }
};

#endif