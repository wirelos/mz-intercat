#include "Sprocket.h"

Sprocket::Sprocket(){
    Serial.println("init sprocket");
}

Sprocket::Sprocket(SprocketConfig cfg){
    config = cfg;
    init(cfg);
}

Sprocket* Sprocket::init(SprocketConfig cfg){
    delay(cfg.startupDelay);
    Serial.begin(cfg.serialBaudRate);
    SPIFFS.begin();
    return this;
}
Sprocket* Sprocket::activate() {
    return activate(&scheduler);
}

Sprocket* Sprocket::join(Network& net){
    Serial.println("join network");
    net.init(&scheduler);
    net.connect();
    return activate(&scheduler, &net);
}

Sprocket* Sprocket::addTask(Task& tsk){
    scheduler.addTask(tsk);
    tsk.enable();
    return this;
}

void Sprocket::loop(){
    scheduler.execute();
}