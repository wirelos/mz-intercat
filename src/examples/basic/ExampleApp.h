#ifndef __EXAMPLE_APP__
#define __EXAMPLE_APP__

//#include "Sprocket.h"
#include <ESPAsyncWebServer.h>
#include <Sprocket.h>

using namespace std;
using namespace std::placeholders;

class ExampleApp : public Sprocket {
    public:
        Task someTask;
        ExampleApp(SprocketConfig cfg) : Sprocket(cfg) {
            Serial.println("joo");
        }
        Sprocket* activate(Scheduler* scheduler) {
            Serial.println("activate");
            someTask.set(TASK_SECOND, TASK_FOREVER, [this](){
                Serial.println("do stuff in task");
            });
            scheduler->addTask(someTask);
            someTask.enable();
        } using Sprocket::activate;
        void server(AsyncWebServer* srv) {
            srv->on("/ping", HTTP_POST, bind(&ExampleApp::handlePingRequest, this, _1));
        }
        void handlePingRequest(AsyncWebServerRequest *request) {
            Serial.println("pinged");
            request->send(200, "text/html", "pong");
        }
};

#endif