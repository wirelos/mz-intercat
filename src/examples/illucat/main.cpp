#include "config.h"
#include "MeshNet.h"
#include "Illucat.h"

MeshNet net({
    STATION_MODE, WIFI_CHANNEL,
    MESH_PORT, MESH_PREFIX, MESH_PASSWORD,
    STATION_SSID, STATION_PASSWORD, HOSTNAME,
    MESH_DEBUG_TYPES
});
Illucat sprocket({ STARTUP_DELAY, SERIAL_BAUD_RATE }, 
    { LED_STRIP_PIN, LED_STRIP_LENGTH });

void setup() {
    sprocket.join(net);
}

void loop() {
    sprocket.loop();
    yield();
}