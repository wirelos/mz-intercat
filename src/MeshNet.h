#ifndef __MESHNET_H__
#define __MESHNET_H__

#include <painlessMesh.h>
#include <WiFiClient.h>
#include "Network.h"

using namespace std;
using namespace std::placeholders;

struct MeshConfig {
    int stationMode;
    int channel;
    int meshPort;
    const char* meshSSID;
    const char* meshPassword;
    const char* stationSSID;
    const char* stationPassword;
    const char* hostname;
    uint16_t debugTypes;
};

class MeshNet : public Network {
    public:
        painlessMesh mesh;
        MeshConfig config; 

        MeshNet(MeshConfig cfg);
        Network* init();
        
        void broadcast(String msg);
        void sendTo(uint32_t target, String msg);
        void update(); // only needed when no scheduler was passed to mesh.init
        void receivedCallback( uint32_t from, String &msg );
        void newConnectionCallback(uint32_t nodeId);
        void changedConnectionCallback();
        void nodeTimeAdjustedCallback(int32_t offset);
};

#endif