#include "MeshNet.h"

MeshNet::MeshNet(MeshConfig cfg) : Network() {
    config = cfg;
}

Network* MeshNet::init(){

    Serial.println("init mesh");
    //mesh.setDebugMsgTypes( ERROR | MESH_STATUS | CONNECTION | SYNC | COMMUNICATION | GENERAL | MSG_TYPES | REMOTE ); // all types on
    mesh.setDebugMsgTypes( config.debugTypes );
    mesh.init( config.meshSSID, config.meshPassword, scheduler, config.meshPort, WIFI_AP_STA, config.channel );
    
    //mesh.onReceive(bind(&MeshNet::receivedCallback,this, _1, _2));
    mesh.onNewConnection(bind(&MeshNet::newConnectionCallback, this, _1));
    mesh.onChangedConnections(bind(&MeshNet::changedConnectionCallback, this));
    mesh.onNodeTimeAdjusted(bind(&MeshNet::nodeTimeAdjustedCallback, this, _1));
    
    if(config.stationMode){
        Serial.println("connect station");
        mesh.stationManual(config.stationSSID, config.stationPassword);
        mesh.setHostname(config.hostname);
    }

    return this;
}

void MeshNet::sendTo(uint32_t target, String msg){
    mesh.sendSingle(target, msg);
}

void MeshNet::broadcast(String msg){
    mesh.sendBroadcast(msg);
}
void MeshNet::update(){
    // only needed when no scheduler was passed to mesh.init
    mesh.update();
}
void MeshNet::receivedCallback( uint32_t from, String &msg ) {
    Serial.printf("--> Received from %u msg=%s\n", from, msg.c_str());
}

void MeshNet::newConnectionCallback(uint32_t nodeId) {
    Serial.printf("--> New Connection, nodeId = %u\n", nodeId);
}

void MeshNet::changedConnectionCallback() {
    Serial.printf("--> Changed connections %s\n",mesh.subConnectionJson().c_str());
}

void MeshNet::nodeTimeAdjustedCallback(int32_t offset) {
    Serial.printf("--> Adjusted time %u. Offset = %d\n", mesh.getNodeTime(),offset);
}