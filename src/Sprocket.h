#ifndef __SPROCKET_H__
#define __SPROCKET_H__

#include <TaskSchedulerDeclarations.h>
#include <Arduino.h>
#include <FS.h>
#include "Network.h"

struct SprocketConfig {
    int startupDelay;
    int serialBaudRate;
};

class Sprocket {
    protected:
        Scheduler scheduler;
    public:
        SprocketConfig config;
        Sprocket();
        Sprocket(SprocketConfig);
        Sprocket* init(SprocketConfig);
        Sprocket* join(Network&);
        Sprocket* addTask(Task&);
        virtual void loop();
        virtual Sprocket* activate();
        virtual Sprocket* activate(Scheduler*) { return this; }
        virtual Sprocket* activate(Scheduler*, Network*) { return this; }
};

#endif