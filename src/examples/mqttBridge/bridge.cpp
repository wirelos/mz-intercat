#include "config.h"
#include "MeshNet.h"
#include "MqttMeshBridge.h"

MeshNet net({
    STATION_MODE, WIFI_CHANNEL,
    MESH_PORT, MESH_PREFIX, MESH_PASSWORD,
    STATION_SSID, STATION_PASSWORD, HOSTNAME,
    MESH_DEBUG_TYPES
});

MqttMeshBridge sprocket(
    { STARTUP_DELAY, SERIAL_BAUD_RATE }, 
    { MQTT_CLIENT_NAME, MQTT_BROKER, MQTT_PORT, MQTT_TOPIC_ROOT }
);

void setup() {
    sprocket.join(net);
}

void loop() {
    sprocket.loop();
    yield();
}