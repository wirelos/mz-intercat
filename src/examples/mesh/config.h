#ifndef __MESH_CONFIG__
#define __MESH_CONFIG__

// Scheduler config
#define _TASK_SLEEP_ON_IDLE_RUN
#define _TASK_STD_FUNCTION

// Chip config
#define SERIAL_BAUD_RATE    115200
#define STARTUP_DELAY       3000

// Mesh config
#define STATION_MODE        0
#define WIFI_CHANNEL        11
#define MESH_PORT           5555
#define MESH_PREFIX         "whateverYouLike"
#define MESH_PASSWORD       "somethingSneaky"
#define STATION_SSID        "Th1ngs4P"
#define STATION_PASSWORD    "th3r31sn0sp00n"
#define HOSTNAME            "mesh-node"
#define MESH_DEBUG_TYPES    ERROR | STARTUP | CONNECTION

#endif