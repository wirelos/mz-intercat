#ifndef __MESH_MQTT_BRIDGE_APP__
#define __MESH_MQTT_BRIDGE_APP__

#include <WiFiClient.h>
#include <painlessMesh.h>
#include <PubSubClient.h>
#include "Sprocket.h"
#include "MeshNet.h"

String MQTT_TOPIC_FROM            = String("/up/") + String(DOMAIN_ID);
String MQTT_TOPIC_FROM_GATEWAY    = String("/up/") + String(DOMAIN_ID) + String("/id/") + String(DEVICE_ID);
String MQTT_TOPIC_TO_ALL          = String("/down/") + String(DOMAIN_ID) + String("/#");

using namespace std;

using namespace std::placeholders;

struct MqttConfig {
    const char* clientName;
    const char* brokerHost;
    int brokerPort;
    const char* topicRoot;
};

class MqttMeshBridge : public Sprocket {
    public:
        MeshNet* net;
        PubSubClient* client;
        WiFiClient wifiClient;
        Task connectTask;
        Task processTask;
        MqttConfig mqttConfig;

        MqttMeshBridge(SprocketConfig sprktCfg, MqttConfig cfg) : Sprocket(sprktCfg) {
            mqttConfig = cfg;
        }

        Sprocket* activate(Scheduler* scheduler){
            enableConnectTask(scheduler);
            enableProcessTask(scheduler);
            return this;
        }

        Sprocket* activate(Scheduler* scheduler, Network* network) {
            Serial.println("activate MQTT bridge");
            net = static_cast<MeshNet*>(network);
            net->mesh.onReceive(bind(&MqttMeshBridge::receivedCallback,this, _1, _2));
            client = new PubSubClient(mqttConfig.brokerHost, mqttConfig.brokerPort,  bind(&MqttMeshBridge::mqttCallback, this, _1, _2, _3), wifiClient);
            return activate(scheduler);
        }

    private:
        void enableConnectTask(Scheduler* scheduler) {
            connectTask.set(TASK_SECOND * 5, TASK_FOREVER, bind(&MqttMeshBridge::connect, this));
            scheduler->addTask(connectTask);
            connectTask.enable();
        }

        void enableProcessTask(Scheduler* scheduler) {
            processTask.set(TASK_MILLISECOND * 5, TASK_FOREVER, bind(&MqttMeshBridge::process, this));                
            scheduler->addTask(processTask);
            processTask.enable();
        }

        void process(){
            client->loop();
        }

        void connect() {
            if (!client->connected()) {
                if (client->connect(mqttConfig.clientName)) {
                    Serial.println("MQTT connected");
                    client->publish(MQTT_TOPIC_FROM_GATEWAY.c_str(),"Ready!");

                    Serial.printf("Subscribing to %s", MQTT_TOPIC_TO_ALL.c_str());
                    client->subscribe(MQTT_TOPIC_TO_ALL.c_str());
                } 
            }
        }

        void receivedCallback( uint32_t from, String &msg ) {
            Serial.printf("bridge: Received from %u msg=%s\n", from, msg.c_str());
            String topic = MQTT_TOPIC_FROM_GATEWAY;
            //String json = "{ \""domain_id\"  \"" + String(from) + "\": \"" + msg + "\"}";

            StaticJsonBuffer<200> jsonFromBuffer;
            JsonObject& jsonFromObj = jsonFromBuffer.parseObject(msg.c_str());
            
            String jsonFromStr;
            jsonFromObj.printTo(jsonFromStr);

            StaticJsonBuffer<200> jsonBuffer;
            JsonObject& root = jsonBuffer.createObject();
            root["domain_id"] = DOMAIN_ID;
            root["from"] = from;
            //root["msg"] = jsonFromObj;
            JsonObject& payload = root.createNestedObject("msg");
            payload["action"] = jsonFromObj["action"];
            payload["severity"] = jsonFromObj["severity"];
            root.printTo(Serial);
            
  String output;
  root.printTo(output);

            Serial.println("publish to " + topic);
            client->publish(topic.c_str(), output.c_str());
        }

        void mqttCallback(char* topic, uint8_t* payload, unsigned int length) {
            char* cleanPayload = (char*)malloc(length+1);
            payload[length] = '\0';
            memcpy(cleanPayload, payload, length+1);
            String msg = String(cleanPayload);
            free(cleanPayload);
            
            Serial.println(topic);
            Serial.println(msg);
            
StaticJsonBuffer<200> jsonBuffer;

JsonObject& root = jsonBuffer.parseObject(msg.c_str());

String targetStr = root["device"];
String fnc = root["fnc"];
//long time          = root["time"];
//double latitude    = root["data"][0];
//double longitude   = root["data"][1];

            //int topicRootLength = String(mqttConfig.topicRoot).length();
            //String targetStr = String(topic).substring(topicRootLength + 4);

            Serial.println("Device: " + targetStr);
            Serial.println("Function: " + fnc);
            /* if(strcmp(targetStr, "gateway"))==0){

            } */
            if(targetStr == "gateway"){
                if(fnc == "getNodes") {
                    Serial.println(net->mesh.subConnectionJson().c_str());
                    client->publish(MQTT_TOPIC_FROM_GATEWAY.c_str(), net->mesh.subConnectionJson().c_str());
                }
            } else if(targetStr == "broadcast") {
                net->mesh.sendBroadcast(msg);
            } else {
                uint32_t target = strtoul(targetStr.c_str(), NULL, 10);
                if(net->mesh.isConnected(target)){
                    net->mesh.sendSingle(target, msg);
                } else {
                    client->publish(MQTT_TOPIC_FROM_GATEWAY.c_str(), "Client not connected!");
                }
            }
        }

};

#endif