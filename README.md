Our entry for [teacher's calling challenge](https://makezurich.ch/images/challenges/challenge-5.png).  
  
Thanks to all for the great time at MakeZürich 2018!  
Due to all the coding, we were a bit lazy on the docs... but soon we'll provide all our thoughts in an easy digestible digital form.  
As all our work is open source, everyone should be able to build a similar or hopefully better system ;)  
So for now, ignore the typos, we'll publish a proper version soon.  
  
# Idea
The goal of this project is to provide a silent alarm system for schools.  
It enables alarm triggers and emergency protocol signaling:  
* alarm trigger remotes controls for teachers and staff to set situation status and notify each other, all classrooms and external agencies  
* led color coded emergency protocol signaling inside and outside the classroom  
* outbound notification to emergency services via internet or LoRaWAN as fallback  
* external monitoring of emergency situations  

# Solution
The system consists of various independant devices that are connected over a self organized mesh network. Each of these devices has a single purpose and propagates messages into the network to notify and trigger functionalities of other devices.  
As of the nature of the mesh, there is no single point of failure that could bring the whole system down;  
when an alarm is triggered, it notifies all other nodes that are directly connected. These in turn, will propagate the notification further until the distress "call" reaches the emergency communication department.  
    
What works:  
* basic remote control for propagating emergency type
* cat based emergency indicator  
* outbound notification over internet via MQTT  

## Emergency Indicator  
To avoid scaring the children in case of an emergency, we use a friendly cat that lights up in different colors inside the classroom, signaling the type and urgency of the incident.  
The teacher can follow an emergency protocol, depending on that indicator.  
The device is powered over USB and has backup battery to ensure full availability, even in case of a power outage.  
  
### Emergency Codes  
The color indicates the type of the emergency.  
* Red = immediate danger  
* Green = situation clear  
* Yellow = save, but need assistance (outside)  
* Orange = need medical assistance  
* Blue = locked & empty (outside classroom)
  
## Remote Control Alarm Trigger 
School staff has a little battery powered device that is capable of triggering alerts.  

## Outbound Notification  
As external emergency services need to be notified in case of an incident, there could be multiple transportation channels.  
* Internet  
* LoRaWAN as fallback  
* any kind of radio transmission...  
  
### MQTT Bridge 
Sends the alarm to a MQTT broker.  
Lorem ipsum

### LoRaWAN Bridge
Broadcasts the alarm to TheThingsNetwork where it is dispatched to the emergency department.  
Dolor sit amet  
  
## Current state
Unfortunately we couldn't get as far as we wanted, so there is no documentation or usefull for the uninitiated.  
We'll add more infos as soon as possible!  
  
Anyway: source code for the devices is available in this git repository.
The different mesh node types can be checked out from the branches (they're inside the examples folder, as we had problems with the library).  
  
# Credits  
Many thanks to the guys of the ChaosDruckerClub that provided us with the illumination-cat; our emergency level indicator.  
[More on the Illumination-Cat](https://www.youtube.com/watch?v=O9ZfBdQorv8)  
  
![intercat](https://pbs.twimg.com/media/Dg88F9FX4AELnQd.jpg)  