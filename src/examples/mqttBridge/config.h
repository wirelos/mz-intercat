#ifndef __BRIDGE_CONFIG__
#define __BRIDGE_CONFIG__

// Scheduler config
#define _TASK_SLEEP_ON_IDLE_RUN
#define _TASK_STD_FUNCTION

// Chip config
#define SERIAL_BAUD_RATE    115200
#define STARTUP_DELAY       3000

// Mesh config
#define STATION_MODE        1
#define WIFI_CHANNEL        11
#define MESH_PORT           5555
#define MESH_PREFIX         "whateverYouLike"
#define MESH_PASSWORD       "somethingSneaky"
#define STATION_SSID        "SSID"
#define STATION_PASSWORD    "PW"
#define HOSTNAME            "sprocket"
#define DOMAIN_ID           "hivemind-domain-id"
#define DEVICE_ID           "gateway"
#define MESH_DEBUG_TYPES    ERROR | STARTUP | CONNECTION

// Bridge config
#define MQTT_CLIENT_NAME    HOSTNAME
#define MQTT_BROKER         "mqtt.hivemind.ch"
#define MQTT_PORT           8883
#define MQTT_TOPIC_ROOT     "mesh"

#endif