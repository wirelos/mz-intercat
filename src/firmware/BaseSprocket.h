#ifndef __BASE_SPROCKET__
#define __BASE_SPROCKET__

#include <ESPAsyncWebServer.h>
#include <Sprocket.h>

using namespace std;
using namespace std::placeholders;

// TODO remove someTask and replace with OTA stuff
class BaseSprocket : public Sprocket {
    public:
        Task someTask;
        MeshNet* net;
        BaseSprocket(SprocketConfig cfg) : Sprocket(cfg) {
            
        }
        Sprocket* activate(Scheduler* scheduler, Network* network) {
            net = static_cast<MeshNet*>(network);
            net->mesh.onReceive(bind(&BaseSprocket::receivedCallback,this, _1, _2));
            // add a task that sends stuff to the mesh
            someTask.set(TASK_SECOND * 5, TASK_FOREVER,
                bind(&BaseSprocket::heartbeat, this, net));
            scheduler->addTask(someTask);
            someTask.enable();
        } using Sprocket::activate;

        void heartbeat(MeshNet* network){
            String msg = "{ \"alive \": 1 }";
            network->broadcast(msg);
        }

        virtual void receivedCallback( uint32_t from, String &msg ) {
            Serial.printf("RECV %u = %s\n", from, msg.c_str());
        }
};

#endif